-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 14 Bulan Mei 2019 pada 09.31
-- Versi server: 10.1.32-MariaDB
-- Versi PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dana_undian`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `campaign`
--

CREATE TABLE `campaign` (
  `id` int(11) UNSIGNED NOT NULL,
  `page` varchar(50) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `desc` text,
  `date_edit` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `show` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `campaign`
--

INSERT INTO `campaign` (`id`, `page`, `nama`, `desc`, `date_edit`, `status`, `img`, `show`) VALUES
(3, NULL, 'sdfas 1231', NULL, NULL, 0, 'campaign-1556752881undian.jpg', 1),
(4, NULL, 'campaign 29 April', NULL, NULL, 0, 'campaign-1556762674undian.jpg', 2),
(5, NULL, 'tes', NULL, NULL, 0, 'campaign-1556790343Rectangle.jpg', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_user`
--

CREATE TABLE `data_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `info_data` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT '0',
  `campaign_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_user`
--

INSERT INTO `data_user` (`id`, `nama`, `kategori`, `info_data`, `status`, `campaign_id`) VALUES
(1, 'satu', 'No Telepon', '1231231', '1', 3),
(4, 'satu', 'Facebook', '1', '1', 4),
(5, 'dua', 'Facebook', ' 2', '1', 4),
(6, 'dua', 'Facebook', 'asdfasdf', '0', 3),
(7, 'tiga', 'Facebook', '3', '1', 4),
(8, 'emapt', 'Facebook', '4', '1', 4),
(9, 'asfas', 'Facebook', '5', '1', 4),
(10, 'sadfsadf', 'Facebook', 'safdasdf', '1', 5),
(11, 'sadfasdf', 'Facebook', '123123123', '1', 5),
(12, 'sadfasdf', 'Facebook', '222', '1', 5),
(13, 'sdafsdafs', 'Facebook', '4444', '1', 5),
(14, 'wdsafadsfasf', 'Facebook', '455555', '1', 5),
(15, 'asfsadfas', 'Facebook', '5665', '1', 5),
(16, 'satu', NULL, '1231231', '0', NULL),
(17, 'satu', NULL, '1', '0', NULL),
(18, 'dua', NULL, '2', '0', NULL),
(19, 'dua', NULL, 'asdfasdf', '0', NULL),
(20, 'tiga', NULL, '3', '0', NULL),
(21, 'emapt', NULL, '4', '0', NULL),
(22, 'asfas', NULL, '5', '0', NULL),
(23, 'sadfsadf', NULL, 'safdasdf', '0', NULL),
(24, 'sadfasdf', NULL, '123123123', '0', NULL),
(25, 'sadfasdf', NULL, '222', '0', NULL),
(26, 'sdafsdafs', NULL, '4444', '0', NULL),
(27, 'wdsafadsfasf', NULL, '455555', '0', NULL),
(28, 'asfsadfas', NULL, '5665', '0', NULL),
(29, 'satu', NULL, '1231231', '1', 5),
(30, 'satu', NULL, '1', '1', 4),
(31, 'dua', NULL, '2', '1', 4),
(32, 'dua', NULL, 'asdfasdf', '1', 4),
(33, 'tiga', NULL, '3', '1', 4),
(34, 'emapt', NULL, '4', '1', 4),
(35, 'asfas', NULL, '5', '1', 4),
(36, 'sadfsadf', NULL, 'safdasdf', '1', 4),
(37, 'sadfasdf', NULL, '123123123', '1', 4),
(38, 'sadfasdf', NULL, '222', '1', 4),
(39, 'sdafsdafs', NULL, '4444', '1', 4),
(40, 'wdsafadsfasf', NULL, '455555', '1', 4),
(41, 'asfsadfas', NULL, '5665', '1', 4),
(42, 'satu', NULL, '1231231', '0', 8),
(43, 'satu', NULL, '1', '0', 8),
(44, 'dua', NULL, '2', '0', 8),
(45, 'dua', NULL, 'asdfasdf', '0', 8),
(46, 'tiga', NULL, '3', '0', 8),
(47, 'emapt', NULL, '4', '0', 8),
(48, 'asfas', NULL, '5', '0', 8),
(49, 'sadfsadf', NULL, 'safdasdf', '0', 8),
(50, 'sadfasdf', NULL, '123123123', '0', 8),
(51, 'sadfasdf', NULL, '222', '0', 8),
(52, 'sdafsdafs', NULL, '4444', '0', 8),
(53, 'wdsafadsfasf', NULL, '455555', '0', 8),
(54, 'asfsadfas', NULL, '5665', '0', 8),
(55, 'satu', NULL, '1231231', '0', 0),
(56, 'satu', NULL, '1', '0', 0),
(57, 'dua', NULL, '2', '0', 0),
(58, 'dua', NULL, 'asdfasdf', '0', 0),
(59, 'tiga', NULL, '3', '0', 0),
(60, 'emapt', NULL, '4', '0', 0),
(61, 'asfas', NULL, '5', '0', 0),
(62, 'sadfsadf', NULL, 'safdasdf', '0', 0),
(63, 'sadfasdf', NULL, '123123123', '0', 0),
(64, 'sadfasdf', NULL, '222', '0', 0),
(65, 'sdafsdafs', NULL, '4444', '0', 0),
(66, 'wdsafadsfasf', NULL, '455555', '0', 0),
(67, 'asfsadfas', NULL, '5665', '0', 0),
(68, 'satu', NULL, '1231231', '0', 0),
(69, 'satu', NULL, '1', '0', 0),
(70, 'dua', NULL, '2', '0', 0),
(71, 'dua', NULL, 'asdfasdf', '0', 0),
(72, 'tiga', NULL, '3', '0', 0),
(73, 'emapt', NULL, '4', '0', 0),
(74, 'asfas', NULL, '5', '0', 0),
(75, 'sadfsadf', NULL, 'safdasdf', '0', 0),
(76, 'sadfasdf', NULL, '123123123', '0', 0),
(77, 'sadfasdf', NULL, '222', '0', 0),
(78, 'sdafsdafs', NULL, '4444', '0', 0),
(79, 'wdsafadsfasf', NULL, '455555', '0', 0),
(80, 'asfsadfas', NULL, '5665', '0', 0),
(81, 'satu', NULL, '1231231', '0', 0),
(82, 'satu', NULL, '1', '0', 0),
(83, 'dua', NULL, '2', '0', 0),
(84, 'dua', NULL, 'asdfasdf', '0', 0),
(85, 'tiga', NULL, '3', '0', 0),
(86, 'emapt', NULL, '4', '0', 0),
(87, 'asfas', NULL, '5', '0', 0),
(88, 'sadfsadf', NULL, 'safdasdf', '0', 0),
(89, 'sadfasdf', NULL, '123123123', '0', 0),
(90, 'sadfasdf', NULL, '222', '0', 0),
(91, 'sdafsdafs', NULL, '4444', '0', 0),
(92, 'wdsafadsfasf', NULL, '455555', '0', 0),
(93, 'asfsadfas', NULL, '5665', '0', 0),
(94, 'satu', NULL, '1231231', '0', 3),
(95, 'satu', NULL, '1', '1', 3),
(96, 'dua', NULL, '2', '0', 3),
(97, 'dua', NULL, 'asdfasdf', '0', 3),
(98, 'tiga', NULL, '3', '0', 3),
(99, 'emapt', NULL, '4', '0', 3),
(100, 'asfas', NULL, '5', '0', 3),
(101, 'sadfsadf', NULL, 'safdasdf', '0', 3),
(102, 'sadfasdf', NULL, '123123123', '0', 3),
(103, 'sadfasdf', NULL, '222', '0', 3),
(104, 'sdafsdafs', NULL, '4444', '0', 3),
(105, 'wdsafadsfasf', NULL, '455555', '0', 3),
(106, 'asfsadfas', NULL, '5665', '0', 3),
(107, 'satu', NULL, '1231231', '0', 3),
(108, 'satu', NULL, '1', '0', 3),
(109, 'dua', NULL, '2', '0', 3),
(110, 'dua', NULL, 'asdfasdf', '0', 3),
(111, 'tiga', NULL, '3', '0', 3),
(112, 'emapt', NULL, '4', '0', 3),
(113, 'asfas', NULL, '5', '0', 3),
(114, 'sadfsadf', NULL, 'safdasdf', '0', 3),
(115, 'sadfasdf', NULL, '123123123', '0', 3),
(116, 'sadfasdf', NULL, '222', '0', 3),
(117, 'sdafsdafs', NULL, '4444', '0', 3),
(118, 'wdsafadsfasf', NULL, '455555', '0', 3),
(119, 'asfsadfas', NULL, '5665', '0', 3),
(120, 'satu', NULL, '1231231', '0', 3),
(121, 'satu', NULL, '1', '0', 3),
(122, 'dua', NULL, '2', '0', 3),
(123, 'dua', NULL, 'asdfasdf', '0', 3),
(124, 'tiga', NULL, '3', '0', 3),
(125, 'emapt', NULL, '4', '0', 3),
(126, 'asfas', NULL, '5', '0', 3),
(127, 'sadfsadf', NULL, 'safdasdf', '0', 3),
(128, 'sadfasdf', NULL, '123123123', '0', 3),
(129, 'sadfasdf', NULL, '222', '0', 3),
(130, 'sdafsdafs', NULL, '4444', '0', 3),
(131, 'wdsafadsfasf', NULL, '455555', '0', 3),
(132, 'asfsadfas', NULL, '5665', '0', 3),
(133, 'satu', NULL, '1231231', '1', 4),
(134, 'satu', NULL, '1', '1', 4),
(135, 'dua', NULL, '2', '1', 4),
(136, 'dua', NULL, 'asdfasdf', '1', 4),
(137, 'tiga', NULL, '3', '1', 4),
(138, 'emapt', NULL, '4', '1', 4),
(139, 'asfas', NULL, '5', '1', 4),
(140, 'sadfsadf', NULL, 'safdasdf', '1', 4),
(141, 'sadfasdf', NULL, '123123123', '1', 4),
(142, 'sadfasdf', NULL, '222', '1', 4),
(143, 'sdafsdafs', NULL, '4444', '1', 4),
(144, 'wdsafadsfasf', NULL, '455555', '1', 4),
(145, 'asfsadfas', NULL, '5665', '1', 4),
(146, 'satu', NULL, '1231231', '1', 5),
(147, 'satu', NULL, '1', '1', 5),
(148, 'dua', NULL, '2', '1', 5),
(149, 'dua', NULL, 'asdfasdf', '1', 5),
(150, 'tiga', NULL, '3', '1', 5),
(151, 'emapt', NULL, '4', '1', 5),
(152, 'asfas', NULL, '5', '1', 5),
(153, 'sadfsadf', NULL, 'safdasdf', '1', 5),
(154, 'sadfasdf', NULL, '123123123', '1', 5),
(155, 'sadfasdf', NULL, '222', '1', 5),
(156, 'sdafsdafs', NULL, '4444', '1', 5),
(157, 'wdsafadsfasf', NULL, '455555', '1', 5),
(158, 'asfsadfas', NULL, '5665', '1', 5),
(159, 'satu', NULL, '1231231', '1', 5),
(160, 'satu', NULL, '1', '1', 5),
(161, 'dua', NULL, '2', '1', 5),
(162, 'dua', NULL, 'asdfasdf', '1', 5),
(163, 'tiga', NULL, '3', '1', 5),
(164, 'emapt', NULL, '4', '1', 5),
(165, 'asfas', NULL, '5', '1', 5),
(166, 'sadfsadf', NULL, 'safdasdf', '1', 5),
(167, 'sadfasdf', NULL, '123123123', '1', 5),
(168, 'sadfasdf', NULL, '222', '1', 5),
(169, 'sdafsdafs', NULL, '4444', '1', 5),
(170, 'wdsafadsfasf', NULL, '455555', '1', 5),
(171, 'asfsadfas', NULL, '5665', '1', 5),
(172, 'satu', NULL, '1231231', '1', 5),
(173, 'satu', NULL, '1', '1', 5),
(174, 'dua', NULL, '2', '1', 5),
(175, 'dua', NULL, 'asdfasdf', '1', 5),
(176, 'tiga', NULL, '3', '1', 5),
(177, 'emapt', NULL, '4', '1', 5),
(178, 'asfas', NULL, '5', '1', 5),
(179, 'sadfsadf', NULL, 'safdasdf', '1', 5),
(180, 'sadfasdf', NULL, '123123123', '1', 5),
(181, 'sadfasdf', NULL, '222', '1', 5),
(182, 'sdafsdafs', NULL, '4444', '1', 5),
(183, 'wdsafadsfasf', NULL, '455555', '1', 5),
(184, 'asfsadfas', NULL, '5665', '1', 5),
(185, 'satu', NULL, '1231231', '1', 5),
(186, 'satu', NULL, '1', '1', 5),
(187, 'dua', NULL, '2', '1', 5),
(188, 'dua', NULL, 'asdfasdf', '1', 5),
(189, 'tiga', NULL, '3', '1', 5),
(190, 'emapt', NULL, '4', '1', 5),
(191, 'asfas', NULL, '5', '1', 5),
(192, 'sadfsadf', NULL, 'safdasdf', '1', 5),
(193, 'sadfasdf', NULL, '123123123', '1', 5),
(194, 'sadfasdf', NULL, '222', '1', 5),
(195, 'sdafsdafs', NULL, '4444', '1', 5),
(196, 'wdsafadsfasf', NULL, '455555', '1', 5),
(197, 'asfsadfas', NULL, '5665', '1', 5),
(198, 'satu', NULL, '1231231', '1', 5),
(199, 'satu', NULL, '1', '1', 5),
(200, 'dua', NULL, '2', '1', 5),
(201, 'dua', NULL, 'asdfasdf', '1', 5),
(202, 'tiga', NULL, '3', '1', 5),
(203, 'emapt', NULL, '4', '1', 5),
(204, 'asfas', NULL, '5', '1', 5),
(205, 'sadfsadf', NULL, 'safdasdf', '1', 5),
(206, 'sadfasdf', NULL, '123123123', '1', 5),
(207, 'sadfasdf', NULL, '222', '1', 5),
(208, 'sdafsdafs', NULL, '4444', '1', 5),
(209, 'wdsafadsfasf', NULL, '455555', '1', 5),
(210, 'asfsadfas', NULL, '5665', '1', 5),
(211, 'satu', NULL, '1231231', '1', 5),
(212, 'satu', NULL, '1', '1', 5),
(213, 'dua', NULL, '2', '1', 5),
(214, 'dua', NULL, 'asdfasdf', '1', 5),
(215, 'tiga', NULL, '3', '1', 5),
(216, 'emapt', NULL, '4', '1', 5),
(217, 'asfas', NULL, '5', '1', 5),
(218, 'sadfsadf', NULL, 'safdasdf', '1', 5),
(219, 'sadfasdf', NULL, '123123123', '1', 5),
(220, 'sadfasdf', NULL, '222', '1', 5),
(221, 'sdafsdafs', NULL, '4444', '1', 5),
(222, 'wdsafadsfasf', NULL, '455555', '1', 5),
(223, 'asfsadfas', NULL, '5665', '1', 5),
(224, 'satu', NULL, '1231231', '1', 5),
(225, 'satu', NULL, '1', '1', 5),
(226, 'dua', NULL, '2', '1', 5),
(227, 'dua', NULL, 'asdfasdf', '1', 5),
(228, 'tiga', NULL, '3', '1', 5),
(229, 'emapt', NULL, '4', '1', 5),
(230, 'asfas', NULL, '5', '1', 5),
(231, 'sadfsadf', NULL, 'safdasdf', '1', 5),
(232, 'sadfasdf', NULL, '123123123', '1', 5),
(233, 'sadfasdf', NULL, '222', '1', 5),
(234, 'sdafsdafs', NULL, '4444', '1', 5),
(235, 'wdsafadsfasf', NULL, '455555', '1', 5),
(236, 'asfsadfas', NULL, '5665', '1', 5),
(237, 'satu', NULL, '1231231', '1', 5),
(238, 'satu', NULL, '1', '1', 5),
(239, 'dua', NULL, '2', '1', 5),
(240, 'dua', NULL, 'asdfasdf', '1', 5),
(241, 'tiga', NULL, '3', '1', 5),
(242, 'emapt', NULL, '4', '1', 5),
(243, 'asfas', NULL, '5', '1', 5),
(244, 'sadfsadf', NULL, 'safdasdf', '1', 5),
(245, 'sadfasdf', NULL, '123123123', '1', 5),
(246, 'sadfasdf', NULL, '222', '1', 5),
(247, 'sdafsdafs', NULL, '4444', '1', 5),
(248, 'wdsafadsfasf', NULL, '455555', '1', 5),
(249, 'asfsadfas', NULL, '5665', '1', 5),
(250, 'satu', NULL, '1231231', '1', 5),
(251, 'satu', NULL, '1', '1', 5),
(252, 'dua', NULL, '2', '1', 5),
(253, 'dua', NULL, 'asdfasdf', '1', 5),
(254, 'tiga', NULL, '3', '1', 5),
(255, 'emapt', NULL, '4', '1', 5),
(256, 'asfas', NULL, '5', '1', 5),
(257, 'sadfsadf', NULL, 'safdasdf', '1', 5),
(258, 'sadfasdf', NULL, '123123123', '1', 5),
(259, 'sadfasdf', NULL, '222', '1', 5),
(260, 'sdafsdafs', NULL, '4444', '1', 5),
(261, 'wdsafadsfasf', NULL, '455555', '1', 5),
(262, 'asfsadfas', NULL, '5665', '1', 5),
(263, 'satu', NULL, '1231231', '1', 5),
(264, 'satu', NULL, '1', '1', 5),
(265, 'dua', NULL, '2', '1', 5),
(266, 'dua', NULL, 'asdfasdf', '1', 5),
(267, 'tiga', NULL, '3', '1', 5),
(268, 'emapt', NULL, '4', '1', 5),
(269, 'asfas', NULL, '5', '1', 5),
(270, 'sadfsadf', NULL, 'safdasdf', '1', 5),
(271, 'sadfasdf', NULL, '123123123', '1', 5),
(272, 'sadfasdf', NULL, '222', '1', 5),
(273, 'sdafsdafs', NULL, '4444', '1', 5),
(274, 'wdsafadsfasf', NULL, '455555', '1', 5),
(275, 'asfsadfas', NULL, '5665', '1', 5),
(276, 'satu', NULL, '1231231', '0', 5),
(277, 'satu', NULL, '1', '1', 5),
(278, 'dua', NULL, '2', '1', 5),
(279, 'dua', NULL, 'asdfasdf', '1', 5),
(280, 'tiga', NULL, '3', '0', 5),
(281, 'emapt', NULL, '4', '0', 5),
(282, 'asfas', NULL, '5', '0', 5),
(283, 'sadfsadf', NULL, 'safdasdf', '0', 5),
(284, 'sadfasdf', NULL, '123123123', '1', 5),
(285, 'sadfasdf', NULL, '222', '0', 5),
(286, 'sdafsdafs', NULL, '4444', '0', 5),
(287, 'wdsafadsfasf', NULL, '455555', '1', 5),
(288, 'asfsadfas', NULL, '5665', '1', 5),
(289, 'satu', NULL, '1231231', '0', 5),
(290, 'satu', NULL, '1', '1', 5),
(291, 'dua', NULL, '2', '0', 5),
(292, 'dua', NULL, 'asdfasdf', '1', 5),
(293, 'tiga', NULL, '3', '0', 5),
(294, 'emapt', NULL, '4', '1', 5),
(295, 'asfas', NULL, '5', '1', 5),
(296, 'sadfsadf', NULL, 'safdasdf', '0', 5),
(297, 'sadfasdf', NULL, '123123123', '1', 5),
(298, 'sadfasdf', NULL, '222', '0', 5),
(299, 'sdafsdafs', NULL, '4444', '0', 5),
(300, 'wdsafadsfasf', NULL, '455555', '0', 5),
(301, 'asfsadfas', NULL, '5665', '0', 5),
(302, 'satu', NULL, '1231231', '0', 5),
(303, 'satu', NULL, '1', '0', 5),
(304, 'dua', NULL, '2', '1', 5),
(305, 'dua', NULL, 'asdfasdf', '0', 5),
(306, 'tiga', NULL, '3', '0', 5),
(307, 'emapt', NULL, '4', '0', 5),
(308, 'asfas', NULL, '5', '0', 5),
(309, 'sadfsadf', NULL, 'safdasdf', '1', 5),
(310, 'sadfasdf', NULL, '123123123', '0', 5),
(311, 'sadfasdf', NULL, '222', '1', 5),
(312, 'sdafsdafs', NULL, '4444', '1', 5),
(313, 'wdsafadsfasf', NULL, '455555', '0', 5),
(314, 'asfsadfas', NULL, '5665', '0', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama`) VALUES
(1, 'No Telepon'),
(2, 'Facebook');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT 'tes',
  `level` varchar(11) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL,
  `status` int(1) NOT NULL,
  `last_login` varchar(33) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `email`, `status`, `last_login`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', 'admin', 0, ''),
(131, 'dani', 'e94d51a35484755a9f9672d13687f499', '0', 'me@danirus.com', 0, '');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `campaign`
--
ALTER TABLE `campaign`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_user`
--
ALTER TABLE `data_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `campaign`
--
ALTER TABLE `campaign`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `data_user`
--
ALTER TABLE `data_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=315;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
