<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');

		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "member";
		$user_id = $this->session->userdata('id');

		$member_query= $this->Modglobal->find('user', array());
		$member = $member_query->result_array();
		$member_num = $member_query->num_rows();

		$level = array (
			'1' => 'Admin', 
			'2' => 'Staff'
		);

		$data = array(
			'content' => 'member/index',
			'member' => $member,
			'member_num' => $member_num,
			//'level' => $level,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add()
	{
		$page = "member";
		$form = "add";
		$user_id = $this->session->userdata('id');

		/*$member_query= $this->Modglobal->find('user', array());
		$member = $member_query->result_array();*/

		$level = array (
			'1' => 'Admin', 
			'2' => 'Staff'
		);
		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'member/form',
			/*'member' => $member,*/
			//'level' => $level,
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}

	public function add_pro()
	{

		$data = array(
        	'username' => $this->input->post('username'),
        	'password' => md5($this->input->post('password')),
        	'email' => $this->input->post('email'),
        	//'level' => $this->input->post('level'),
        	'status' => $this->input->post('status'),
        );
        $this->Modglobal->insert('user', $data);
		redirect('member');
	}
	public function edit()
	{
		$page = "member";
		$form = "edit";
		$user_id = $this->session->userdata('id');

		$id = $this->uri->segment(3);

		$member_query= $this->Modglobal->find('user', array('id' => $id));
		$member = $member_query->row_array();

		$level = array (
			'1' => 'Admin', 
			'2' => 'Staff'
		);
		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'member/form',
			'member' => $member,
			'level' => $level,
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}

	public function update()
	{
		if($this->input->post('password')) {
			$password = md5($this->input->post('password'));
		}
		else {
			$password = $this->input->post('password2');
		}

		$data = array(
        	'username' => $this->input->post('username'),
        	'password' => $password,
        	'email' => $this->input->post('email'),
        	'level' => $this->input->post('level'),
        	'status' => $this->input->post('status'),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('user', $data, $where);
		redirect('member');
	}

	public function delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('user', $where);
		//echo $id;

		redirect('member/');
	}
	
	
	
	
}


