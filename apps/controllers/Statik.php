<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statik extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Statik";

		$id = $this->uri->segment(3);

		$page_query = $this->Modglobal->find('statik', array('id' => '1'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'hubungikami/form',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function success()
	{
		$page = "Statik";
		$cate = "success";
		$title = "Success Page Wording";

		$page_query = $this->Modglobal->find('statik', array('id' => '2'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'statik/form',
			'page' => $page,
			'title' => $title,
			'cate' => $cate,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	public function fail()
	{
		$page = "Statik";
		$cate = "fail";
		$title = "Fail Page Wording";

		$page_query = $this->Modglobal->find('statik', array('id' => '3'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'statik/form',
			'page' => $page,
			'title' => $title,
			'cate' => $cate,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	public function giftbox()
	{
		$page = "Statik";
		$cate = "giftbox";
		$title = "Gift Box Page Wording";

		$page_query = $this->Modglobal->find('statik', array('id' => '4'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'statik/form',
			'page' => $page,
			'title' => $title,
			'cate' => $cate,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	public function winpage()
	{
		$page = "Statik";
		$cate = "winpage";
		$title = "Win Prize Page Wording";

		$page_query = $this->Modglobal->find('statik', array('id' => '5'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'statik/form',
			'page' => $page,
			'title' => $title,
			'cate' => $cate,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	public function notlucky()
	{
		$page = "Statik";
		$cate = "notlucky";
		$title = "Not Lucky Page Wording";

		$page_query = $this->Modglobal->find('statik', array('id' => '7'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'statik/form',
			'page' => $page,
			'title' => $title,
			'cate' => $cate,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	public function done()
	{
		$page = "Statik";
		$cate = "done";
		$title = "Done Page Wording";

		$page_query = $this->Modglobal->find('statik', array('id' => '6'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'statik/form',
			'page' => $page,
			'title' => $title,
			'cate' => $cate,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	public function home()
	{
		$page = "Statik";
		$cate = "home";
		$title = "Home Wording";

		$page_query = $this->Modglobal->find('statik', array('id' => '1'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'statik/form',
			'page' => $page,
			'title' => $title,
			'cate' => $cate,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function update(){
		$data = array(
        	'judul' => str_replace_text($this->input->post('judul')),
        	'desc' => str_replace_text($this->input->post('desc')),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('statik', $data, $where);
		redirect('statik/'.$this->input->post('cate'));
	}
}


