<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Prize";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('campaign', array());
		$result = $query->result_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$show = array (
			'0' => 'Name and User ID', 
			'1' => 'Name Only',
			'2' => 'User ID Only'
		);

		$data = array(
			'content' => 'campaign/index',
			'result' => $result,
			'status' => $status,
			'show' => $show,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function peserta()
	{
		$page = "Campaign";
		$id = $this->uri->segment(3);
		$namaalbum = $this->uri->segment(5);

		$page_query = $this->Modglobal->find('campaign', array('id' => $id));
		$page_detail = $page_query->row_array();

		$list_query= $this->Modglobal->find('data_user', array('campaign_id' => $id));
		$list = $list_query->result_array();
		$num = $list_query->num_rows();
		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'campaign/index_peserta',
			'page' => $page,
			'page_detail' => $page_detail,
			'list' => $list,
			'id' => $id,
			'num' => $num,
			'status' => $status,
		);
		$this->load->view('layouts/base', $data);
	}
	public function pemenang()
	{
		$page = "Campaign";
		$id = $this->uri->segment(3);
		$namaalbum = $this->uri->segment(5);

		$page_query = $this->Modglobal->find('campaign', array('id' => $id));
		$page_detail = $page_query->row_array();

		$list_query= $this->Modglobal->find('data_user', array('campaign_id' => $id, 'status' => '1'));
		$list = $list_query->result_array();
		$num = $list_query->num_rows();
		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'campaign/index_pemenang',
			'page' => $page,
			'page_detail' => $page_detail,
			'list' => $list,
			'id' => $id,
			'num' => $num,
			'status' => $status,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add()
	{
		$page = "Campaign";
		$form = "add";
		$user_id = $this->session->userdata('id');


		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$show = array (
			'0' => 'Name and User ID', 
			'1' => 'Name Only',
			'2' => 'User ID Only'
		);

		$data = array(
			'content' => 'campaign/form',
			'page' => $page,
			'form' => $form,
			'status' => $status,
			'show' => $show,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add_pro()
	{
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'campaign-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = 'media/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }

		$data = array(
        	'nama' => $this->input->post('nama'),
        	'status' => $this->input->post('status'),
        	'show' => $this->input->post('show'),
        	'img' => $img,
        );
        $this->Modglobal->insert('campaign', $data);
		redirect('campaign');
	}
	public function edit()
	{
		$page = "Campaign";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('campaign', array('id' => $id));
		$page_detail = $query->row_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$show = array (
			'0' => 'Name and User ID', 
			'1' => 'Name Only',
			'2' => 'User ID Only'
		);

		$data = array(
			'content' => 'campaign/form',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
			'status' => $status,
			'show' => $show,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function update(){
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'campaign-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = 'media/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }
	    
		$data = array(
			'nama' => $this->input->post('nama'),
        	'status' => $this->input->post('status'),
        	'show' => $this->input->post('show'),
        	'img' => $img,
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('campaign', $data, $where);
		redirect('campaign');
	}
	public function delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('campaign', $where);
		//echo $id;

		redirect('campaign');
	}

	public function add_peserta()
	{
		$page = "Peserta";
		$form = "add";
		$user_id = $this->session->userdata('id');
		$id_campaign = $this->uri->segment(3);

		$query = $this->Modglobal->find('kategori', array(),'nama');
		$kategori = $query->result_array();


		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'campaign/form_peserta',
			'page' => $page,
			'form' => $form,
			'status' => $status,
			'id_campaign' => $id_campaign,
			'kategori' => $kategori,
		);
		$this->load->view('layouts/base', $data);
	}
	public function addpeserta_pro()
	{
		$info_data = str_replace(" ","",$this->input->post('info_data'));
		$data = array(
        	'nama' => $this->input->post('nama'),
        	'kategori' => $this->input->post('kategori'),
        	'info_data' => $info_data,
        	'campaign_id' => $this->input->post('campaign_id'),
        );
        $this->Modglobal->insert('data_user', $data);
		redirect('campaign/peserta/'.$this->input->post('campaign_id'));
	}
	public function edit_peserta()
	{
		$page = "Peserta";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('data_user', array('id' => $id));
		$page_detail = $query->row_array();

		$query_kategori = $this->Modglobal->find('kategori', array(),'nama');
		$kategori = $query_kategori->result_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'campaign/form_peserta',
			'page' => $page,
			'page_detail' => $page_detail,
			'kategori' => $kategori,
			'id' => $id,
			'status' => $status,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function updatepeserta(){

		$info_data = str_replace(" ","",$this->input->post('info_data'));
		
		$data = array(
			'nama' => $this->input->post('nama'),
        	'kategori' => $this->input->post('kategori'),
        	'info_data' => $info_data,
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('data_user', $data, $where);
		//redirect('campaign');
		redirect('campaign/peserta/'.$this->input->post('campaign_id'));
	}
	public function delete_peserta() {
		$id = $this->uri->segment(3);
		$campaign_id = $this->uri->segment(4);

		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('data_user', $where);

		redirect('campaign/peserta/'.$campaign_id);
	}
	public function detail()
	{
		$page = "Detail";
		$id = $this->uri->segment(3);
		$namaalbum = $this->uri->segment(5);

		$page_query = $this->Modglobal->find('campaign', array('id' => $id));
		$page_detail = $page_query->row_array();

		$list_query= $this->Modglobal->find('data_user', array('campaign_id' => $id));
		$list = $list_query->result_array();
		$num = $list_query->num_rows();
		
		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'campaign/detail',
			'page' => $page,
			'page_detail' => $page_detail,
			'list' => $list,
			'id' => $id,
			'num' => $num,
			'status' => $status,
		);
		$this->load->view('layouts/polos', $data);
	}
	public function result()
	{
		$page = "Detail";
		$id = $this->uri->segment(3);
		$namaalbum = $this->uri->segment(5);

		$list_query= $this->Modglobal->find('data_user', array('campaign_id' => $id), 'rand()');
		$pemenang = $list_query->row_array();
		
		$data = array(
			'content' => 'campaign/result',
			'page' => $page,
			'pemenang' => $pemenang,
			'id' => $id,
		);
		$this->load->view('layouts/kosong', $data);
	}
	public function get_name()
	{
		$page = "Detail";
		$info_data = $this->uri->segment(3);

		$nama = str_replace(" ","",$info_data);

		$list_query= $this->Modglobal->find('data_user', array('info_data' => $nama), 'rand()');
		//$list_query= $this->Modglobal->find('data_user', array(),'',array('info_data' => $nama));
		$nama = $list_query->row_array();
		
		$data = array(
			'content' => 'campaign/get_name',
			'page' => $page,
			'nama' => $nama,
		);
		$this->load->view('layouts/kosong', $data);
	}
	public function importcsv()
	{
		$fileName = $_FILES["file"]["tmp_name"];

	    $campaign_id = $this->input->post('campaign_id');
	    
	    if ($_FILES["file"]["size"] > 0) {
	        
	        $file = fopen($fileName, "r");
	        
	        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
	        	$info_data = str_replace(" ","",$column[1]);
	        	$info_data = str_replace("	","",$info_data);
	        	$data = array(
		        	'nama' => $column[0],
		        	'info_data' => $info_data,
		        	'campaign_id' => $campaign_id,
		        );
		        $this->Modglobal->insert('data_user', $data);
	        }
	    }
	    redirect('campaign/peserta/'.$this->input->post('campaign_id'));
	}
	
}


