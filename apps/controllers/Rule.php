<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rule extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Rule";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('rule', array());
		$result = $query->result_array();

		$query_prize= $this->Modglobal->find('prize', array());
		$prize = $query_prize->result_array();

		$data = array(
			'content' => 'rule/form',
			'result' => $result,
			'prize' => $prize,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function update(){

		// if($this->input->post('desc') == $this->input->post('desc2')) {
		// }
		// else {
		
		// 	$data = array(
		// 		'desc' => $this->input->post('desc'),
	 //        );
	 //        $where = array(
	 //    		'id' => $this->input->post('id'),
	 //        );
	 //        $this->Modglobal->update('statik', $data, $where);


	 //        $id = $this->uri->segment(3);
		// 	$where = array(
	 //        	'id' => $id,
	 //        );
		// 	$this->Modglobal->delete('prize', $where);

		// 	for ($x = 0; $x <= 10; $x++) {
		// 	    echo "The number is: $x <br>";
		// 	} 
		// }
		$where = array(
        	'delete_id' => '0',
        );
		$this->Modglobal->delete('rule', $where);

		for($i=0; $i< count($this->input->post('id')); $i++)
	    { 
	    	$data = array(
	        	'prize_id' => $this->input->post('prize_id')[$i]
	        );
	        $this->Modglobal->insert('rule', $data);
	    }
		redirect('rule');
	}
	public function set()
	{
		$page = "Rule";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('statik', array('id' => '8'));
		$page_detail = $query->row_array();

		$data = array(
			'content' => 'rule/form',
			'page_detail' => $page_detail,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	
}


