<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');

		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Kategori";
		$user_id = $this->session->userdata('id');

		$kategori_query= $this->Modglobal->find('kategori', array());
		$kategori = $kategori_query->result_array();
		$kategori_num = $kategori_query->num_rows();

		
		$data = array(
			'content' => 'kategori/index',
			'kategori' => $kategori,
			'kategori_num' => $kategori_num,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add()
	{
		$page = "Kategori";
		$form = "add";
		$user_id = $this->session->userdata('id');

		/*$member_query= $this->Modglobal->find('user', array());
		$member = $member_query->result_array();*/

		$data = array(
			'content' => 'kategori/form',
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}

	public function add_pro()
	{

		$data = array(
        	'nama' => $this->input->post('nama'),
        );
        $this->Modglobal->insert('kategori', $data);
		redirect('kategori');
	}
	public function edit()
	{
		$page = "Kategori";
		$form = "edit";
		$user_id = $this->session->userdata('id');

		$id = $this->uri->segment(3);

		$kategori_query= $this->Modglobal->find('kategori', array('id' => $id));
		$page_detail = $kategori_query->row_array();

		$data = array(
			'content' => 'kategori/form',
			'page_detail' => $page_detail,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}

	public function update()
	{
		
		$data = array(
        	'nama' => $this->input->post('nama'),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('kategori', $data, $where);
		redirect('kategori');
	}

	public function delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('kategori', $where);
		//echo $id;

		redirect('kategori');
	}
	
	
	
	
}


