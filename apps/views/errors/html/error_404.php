<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	color: #4F5155;
	text-align: center;
	font-weight: bold;
	font-family: helvetica, arial;
}
h1 {
	font-size: 200px;
	margin: 0;
	padding: 0;
	line-height: 100%;
}
.logo {
	width: 150px;
}
.btn {
	position: relative;
	border-radius: 10px;
	padding: 10px 30px;
	background: orange;
	color: #fff;
	font-weight: bold;
}
.btn:hover {
	opacity: 0.8;
}
</style>
</head>
<body>
	<h1>404</h1>
	Halaman tidak ditemukan
	<br><br><br><br>
	<a href="/" class="btn">Kembali ke beranda</a>
	
</body>
</html>