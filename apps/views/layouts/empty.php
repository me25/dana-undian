<!doctype html>	
<html>
<head>
	<?php $this->load->view('includes/head')?>	
</head>

<?php
if (isset($content) && !empty($content)) {
  $this->load->view($content);
}
?>
<div class="clearfix"></div>
</body>
</html>