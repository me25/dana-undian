
<div class="content_ful">
	<div class="table_show">
		<?php
			if($form == "add"){
				$title = 'Add '.$page;
				$action = 'campaign/add_pro';
			}
			else{
				$title = 'Edit '.$page;
				$action = 'campaign/update';
			}
		?>
		<div class="table_head">
			<div class="info"><h2><?php echo $title;?></h2></div>
			<div class="clearfix"></div>
		</div>


		<hr color="#eee">
		<form action="<?php echo base_url($action);?>" class="form_1" method="post"  enctype="multipart/form-data">
			<div class="form-group">
		      	<strong>Nama Campaign</strong>
		      	<input type="hidden" name="id" value="<?php echo $page_detail['id'];?>">
		      	<input type="text" name="nama" value="<?php echo $page_detail['nama'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Image</strong>
		      	<input type="file" name="img" id="uploadFile">
		      	<input type="hidden" name="img2" value="<?php echo $page_detail['img'];?>">
		      	<h6>Image Size max:520x80px (rasio 4:1), max file size 500kb</h6>
		      	<div id="imagePreview">
		      		<br>
		      		<?php
		      			if($page_detail['img']){
		      				echo '<img src="../../media/'.$page_detail['img'].'" alt="" height="80">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		    	<strong>Show Data</strong>
		      	<div class="select-style">
					<span></span>
					<select name="show" id="" required="">
						<?php
						foreach ($show as $i => $row) {
							if($page_detail['show'] == $i){
								echo '<option value="'.$i.'" selected>'.$row.'</option>';
							}
							else{
								echo '<option value="'.$i.'">'.$row.'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		    	<strong>Status</strong>
		      	<div class="select-style">
					<span></span>
					<select name="status" id="" required="">
						<?php
						foreach ($status as $i => $row_status) {
							if($page_detail['status'] == $i){
								echo '<option value="'.$i.'" selected>'.$row_status.'</option>';
							}
							else{
								echo '<option value="'.$i.'">'.$row_status.'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <br>
		    <div>
		    	<a href="<?php echo base_url('campaign');?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<script>
	$(function() {
	    $("#uploadFile").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
</script>