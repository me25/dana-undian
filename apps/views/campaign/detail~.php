<div class="body_front" style="background-image: url(<?php echo base_url('media/').$page_detail['img'];?>)">
    <div class="logo">
        <img src="<?php echo assets_url('images');?>/logo.png" alt="">
    </div>
    <div class="text">
        <h4 id="output1">Nama Pemenang</h4>
        <h2 class="awal" id="output0">
            xxxxxxxxx
        </h2>
        <div class="hasil"></div>
    </div>
    <!-- <div id="enter">enter</div>
    <div id="stop">stop</div>
    <?php echo $id;?>
    <div id="mydiv">tesssss</div> -->


</div>
<script>
function generateNumber(index,durasi,hasil) {
  var quotes = new Array("087851423695","08283390410","085655030705","085730667835", "087850605356", "085231770004", "087850032365","081330010166","08977279920","081703770047","087851111728","085733488500"), 
  
  randno = quotes[Math.floor( Math.random() * quotes.length )];

  var duration = 500;

  var output = $('#output' + index); // Start ID with letter
  var started = new Date().getTime();


  $('#output' + index).numAnim({
        endAt: hasil,
        duration: durasi
  });
}

(function($){
    $.fn.extend({
        numAnim: function(options) {
            if ( ! this.length)
                return false;

            this.defaults = {
                endAt: 2560,
                numClass: 'autogen-num',
                duration: 5,   // seconds
                interval: 90  // ms
            };
            var settings = $.extend({}, this.defaults, options);

            var $num = $('<span/>', {
                'class': settings.numClass 
            });

            return this.each(function() {
                var $this = $(this);

                // Wrap each number in a tag.
                var frag = document.createDocumentFragment(),
                    numLen = settings.endAt.toString().length;
                for (x = 0; x < numLen; x++) {
                    //var rand_num = Math.floor( Math.random() * 10 );
                    //frag.appendChild( $num.clone().text(rand_num)[0] )

                    var rand_num = Math.floor( Math.random() * 10 );
                    frag.appendChild( $num.clone().text(rand_num)[0] )
                }
                $this.empty().append(frag);

                var get_next_num = function(num) {
                    ++num;
                    if (num > 9) return 0;
                    return num;
                };

                // Iterate each number.
                $this.find('.' + settings.numClass).each(function() {
                    var $num = $(this),
                        num = parseInt( $num.text() );

                    var interval = setInterval( function() {
                        num = get_next_num(num);
                        $num.text(num);
                    }, settings.interval);

                    setTimeout( function() {
                        clearInterval(interval);
                    }, settings.duration * 1000 - settings.interval);
                });

                setTimeout( function() {
                    $this.text( settings.endAt.toString() );
                }, settings.duration * 1000);
            });
        }
    });
})(jQuery);


/*$("#enter").click(function(){
    generateNumber(0,'900');
});

$("#stop").click(function(){
     $.get("<?php echo base_url('campaign/result/').$id;?>", function(data){
           var hasil = data;
           //alert(hasil);
           generateNumber(0,'0',hasil);
    });
    generateNumber(0,'0');
});*/

$(document).keydown(function(e) {
    switch (e.which) {
    case 13:
        $.get("<?php echo base_url('campaign/result/').$id;?>", function(data){
               var hasil = data;
               //alert(hasil);
               generateNumber(0,'1',hasil);

               $.get("<?php echo base_url('campaign/get_name/');?>" + hasil, function(nama){
                       var nama = nama;
                       //alert(hasil);
                       generateNumber(1,'0.5',nama);
                });
        });
        //generateNumber(0,'0');
        //alert();
        break;
    case 32:
        generateNumber(1,'90');
        generateNumber(0,'90');
        //alert();
        break;
    }

});

</script>