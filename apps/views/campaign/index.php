
<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info">Campaign</div>
			<a href="<?php echo base_url('campaign/add');?>" class="btn_add">+ Tambah</a>
			<div class="clearfix"></div>
		</div>
		<table id="table_sort" class="table_style" cellspacing="0" width="100%"data-page-length="10" >
		    <thead>
		        <tr>
		            <th width="20%">Nama Campaign</th>
		            <th>Image</th>
		            <th>Peserta</th>
		            <th>Campaign</th>
		            <th>Pemenang</th>
		            <th>Show Data</th>
		            <th>Status</th>
		            <th width="50px" class="arrow_non">Action</th>
		        </tr>
		    </thead>
		    <tbody>
		    	<?php foreach ($result as $row) { 
		    		?>
		    		<tr>
			            <td>
			            	<?php echo $row['nama'];?>
			            </td>
			            <td>
			            	<?php
				      			if($row['img']){
				      				echo '<img src="media/'.$row['img'].'" alt="" height="100">';
				      			}
				      		?>
			            </td>
			            <td>
			            	<a href="<?php echo base_url('campaign/peserta/').$row['id'];?>" class="btn_lihat">Lihat</a>
			            </td>
			            <td>
			            	<a href="<?php echo base_url('campaign/detail/').$row['id'];?>" class="btn_lihat" target="_blank">Run Campaign</a>
			            </td>
			            <td>
			            	<a href="<?php echo base_url('campaign/pemenang/').$row['id'];?>" class="btn_lihat">Pemenang</a>
			            </td>
			            <td><?php echo $show[$row['show']];?></td>
			            <td><?php echo $status[$row['status']];?></td>
			            <td class="action">
			            	
			            	<a href="<?php echo base_url('campaign/edit/').$row['id'];?>"><img src="<?php echo assets_url('images');?>/ico_edit.png" alt=""></a>
			            	<a href="<?php echo base_url('campaign/delete/').$row['id'];?>" class="delete"><img src="<?php echo assets_url('images');?>/ico_delete.png" alt=""></a>
			            </td>
			        </tr>

		    		<?php

		    	}?>
		        
		        
		        
		    </tbody>
		</table>
	</div>
</div>