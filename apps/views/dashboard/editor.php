
<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info"><h2>Home Editor</h2></div>
			<div class="clearfix"></div>
		</div>


		<hr color="#eee">
		<form action="<?php echo base_url('home/update');?>" class="form_1" method="post"  enctype="multipart/form-data">
			<div class="head_section">
				<h2>Cover</h2>
			</div>
			<div class="form-group">
		      	<strong>Image Cover</strong>
		      	<input type="file" name="cover" id="uploadFile">
		      	<input type="hidden" name="cover2" value="<?php echo $page_detail['cover'];?>">
		      	<div id="imagePreview">
		      		<?php
		      			if($page_detail['cover']){
		      				echo '<img src="../../uploads/web/'.$page_detail['cover'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Title</strong>
		      	<input type="text" name="title" value="<?php echo $page_detail['title'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Sub Title</strong>
		      	<input type="text" name="subtitle" value="<?php echo $page_detail['subtitle'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="head_section">
		    	<br><br><br>
				<h2>Why fed insight?</h2>
			</div>
			<div class="form-group">
		      	<strong>Image</strong>
		      	<input type="file" name="img_why" id="uploadFile2">
		      	<input type="hidden" name="img_why2" value="<?php echo $page_detail['img_why'];?>">
		      	<div id="imagePreview2">
		      		<?php
		      			if($page_detail['img_why']){
		      				echo '<img src="../../uploads/web/'.$page_detail['img_why'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Content</strong>
		      	<textarea name="why" id="" cols="30" rows="10" class="tinymc"><?php echo $page_detail['why'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <!-- <div class="head_section">
		    	<br><br><br>
				<h2>What we provide?</h2>
			</div>
			<div class="form-group">
		      	<strong>Service 1</strong>
		      	<input type="text" name="service1" value="<?php echo $page_detail['service1'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Description</strong>
		      	<textarea name="service1_desc" id="" cols="30" rows="2"><?php echo $page_detail['service1_desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
			<div class="form-group">
		      	<strong>Service 1 Image</strong>
		      	<input type="file" name="service1_img" id="uploadFile3">
		      	<input type="hidden" name="service1_img2" value="<?php echo $page_detail['service1_img'];?>">
		      	<div id="imagePreview3">
		      		<?php
		      			if($page_detail['service1_img']){
		      				echo '<img src="../../uploads/web/'.$page_detail['service1_img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Service 2</strong>
		      	<input type="text" name="service2" value="<?php echo $page_detail['service2'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Description</strong>
		      	<textarea name="service2_desc" id="" cols="30" rows="2"><?php echo $page_detail['service2_desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
			<div class="form-group">
		      	<strong>Service 2 Image</strong>
		      	<input type="file" name="service2_img" id="uploadFile4">
		      	<input type="hidden" name="service2_img2" value="<?php echo $page_detail['service2_img'];?>">
		      	<div id="imagePreview4">
		      		<?php
		      			if($page_detail['service2_img']){
		      				echo '<img src="../../uploads/web/'.$page_detail['service2_img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Service 3</strong>
		      	<input type="text" name="service3" value="<?php echo $page_detail['service3'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Description</strong>
		      	<textarea name="service3_desc" id="" cols="30" rows="2"><?php echo $page_detail['service3_desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
			<div class="form-group">
		      	<strong>Service 3 Image</strong>
		      	<input type="file" name="service3_img" id="uploadFile5">
		      	<input type="hidden" name="service3_img2" value="<?php echo $page_detail['service3_img'];?>">
		      	<div id="imagePreview5">
		      		<?php
		      			if($page_detail['service3_img']){
		      				echo '<img src="../../uploads/web/'.$page_detail['service3_img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Service 4</strong>
		      	<input type="text" name="service4" value="<?php echo $page_detail['service4'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Description</strong>
		      	<textarea name="service4_desc" id="" cols="30" rows="2"><?php echo $page_detail['service4_desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
			<div class="form-group">
		      	<strong>Service 4 Image</strong>
		      	<input type="file" name="service4_img" id="uploadFile6">
		      	<input type="hidden" name="service4_img2" value="<?php echo $page_detail['service4_img'];?>">
		      	<div id="imagePreview6">
		      		<?php
		      			if($page_detail['service4_img']){
		      				echo '<img src="../../uploads/web/'.$page_detail['service4_img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div> -->
		    <div class="head_section">
		    	<br><br><br><br>
				<h2>How we work?</h2>
			</div>
			<div class="form-group">
		      	<strong>Description</strong>
		      	<textarea name="how" id="" cols="30" rows="2"><?php echo $page_detail['how'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>How 1</strong>
		      	<input type="text" name="how1" value="<?php echo $page_detail['how1'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Description</strong>
		      	<textarea name="how1_desc" id="" cols="30" rows="2"><?php echo $page_detail['how1_desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
			<div class="form-group">
		      	<strong>How 1 Image</strong>
		      	<input type="file" name="how1_img" id="uploadFile7">
		      	<input type="hidden" name="how1_img2" value="<?php echo $page_detail['how1_img'];?>">
		      	<div id="imagePreview7">
		      		<?php
		      			if($page_detail['how1_img']){
		      				echo '<img src="../../uploads/web/'.$page_detail['how1_img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>How 2</strong>
		      	<input type="text" name="how2" value="<?php echo $page_detail['how2'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Description</strong>
		      	<textarea name="how2_desc" id="" cols="30" rows="2"><?php echo $page_detail['how2_desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
			<div class="form-group">
		      	<strong>How 2 Image</strong>
		      	<input type="file" name="how2_img" id="uploadFile8">
		      	<input type="hidden" name="how2_img2" value="<?php echo $page_detail['how2_img'];?>">
		      	<div id="imagePreview8">
		      		<?php
		      			if($page_detail['how2_img']){
		      				echo '<img src="../../uploads/web/'.$page_detail['how2_img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>How 3</strong>
		      	<input type="text" name="how3" value="<?php echo $page_detail['how3'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Description</strong>
		      	<textarea name="how3_desc" id="" cols="30" rows="2"><?php echo $page_detail['how3_desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
			<div class="form-group">
		      	<strong>How 3 Image</strong>
		      	<input type="file" name="how3_img" id="uploadFile9">
		      	<input type="hidden" name="how3_img2" value="<?php echo $page_detail['how3_img'];?>">
		      	<div id="imagePreview9">
		      		<?php
		      			if($page_detail['how3_img']){
		      				echo '<img src="../../uploads/web/'.$page_detail['how3_img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="head_section">
		    	<br><br><br><br>
				<h2>Our Client</h2>
			</div>
			<div class="form-group">
		      	<strong>Description</strong>
		      	<textarea name="client" id="" cols="30" rows="2"><?php echo $page_detail['client'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <br>
		    <div>
		    	<a href="<?php echo base_url('member');?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>
<script>
	$(function() {
	    $("#uploadFile").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
	$(function() {
	    $("#uploadFile2").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview2").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
	$(function() {
	    $("#uploadFile3").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview3").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
	$(function() {
	    $("#uploadFile4").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview4").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
	$(function() {
	    $("#uploadFile5").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview5").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
	$(function() {
	    $("#uploadFile6").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview6").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
	$(function() {
	    $("#uploadFile7").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview7").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
	$(function() {
	    $("#uploadFile8").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview8").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
	$(function() {
	    $("#uploadFile9").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview9").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
</script>