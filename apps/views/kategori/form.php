
<div class="content_ful">
	<div class="table_show">
		<?php
			if($form == "add"){
				$title = "Add Kategori";
				$action = 'kategori/add_pro';
			}
			else{
				$title = "Edit Kategori";
				$action = 'kategori/update';
			}
		?>
		<div class="table_head">
			<div class="info"><h2><?php echo $title;?></h2></div>
			<div class="clearfix"></div>
		</div>


		<hr color="#eee">
		<form action="<?php echo base_url($action);?>" class="form_1" method="post">
		    <div class="form-group form-group-col-2">
		      	<strong>Nama Kategori</strong>
		      	<input type="hidden" name="id" value="<?php echo $page_detail['id'];?>">
		      	<input type="text" name="nama" value="<?php echo $page_detail['nama'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		   
		    <br>
		    <div>
		    	<a href="<?php echo base_url('kategori');?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<div id="pop_box2" class="pop_box" style="display:none;">
	<div class="popbox_bg_close"></div>
	
</div>