


<div class="content_ful">
	<div class="table_head">
			<h1>Tentang Dana</h1>
		</div>
	<div class="table_show">
		<form action="<?php echo base_url('statik/tentangupdate');?>" class="form_1" method="post" enctype="multipart/form-data">
		    <div class="form-group">
		      	<strong>Nama</strong>
		      	<input type="text" name="nama" value="<?php echo $page_detail['nama'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Teaser Text</strong>
		      	<textarea name="teaser" id="" cols="30" rows="10" class="tinymc"><?php echo $page_detail['teaser'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		   
		    <div class="form-group">
		      	<strong>Content</strong>
		      	<textarea name="desc" id="" cols="30" rows="10" class="tinymc"><?php echo $page_detail['desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Cover Image</strong>
		      	<input type="file" name="img" id="uploadFile">
		      	<input type="hidden" name="img2" value="<?php echo $page_detail['img'];?>">
		      	<div id="imagePreview">
		      		<?php
		      			if($page_detail['img']){
		      				echo '<img src="../../uploads/web/'.$page_detail['img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Background Image</strong>
		      	<input type="file" name="img_bg" id="uploadFile2">
		      	<input type="hidden" name="img_bg2" value="<?php echo $page_detail['img2'];?>">
		      	<div id="imagePreview2">
		      		<?php
		      			if($page_detail['img2']){
		      				echo '<img src="../../uploads/web/'.$page_detail['img2'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div>
		    	<!-- <a href="<?php echo base_url('about');?>" class="btn_cancel close_box">CANCEL</a> -->
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<script>
	$(function() {
	    $("#uploadFile").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	    $("#uploadFile2").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview2").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });

	});
</script>