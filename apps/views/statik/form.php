

<div class="content_ful">
	<div class="table_head">
			<h1><?php echo $title;?></h1>
		</div>
	<div class="table_show">
		<form action="<?php echo base_url('statik/update');?>" class="form_1" method="post" enctype="multipart/form-data">
		    <div class="form-group">
		      	<strong>Title</strong>
		      	<input type="hidden" name="id" value="<?php echo $page_detail['id'];?>" required="required">
		      	<input type="hidden" name="cate" value="<?php echo $cate;?>" required="required">
		      	<input type="text" name="judul" value="<?php echo $page_detail['judul'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>

		    <?php
		    	if($cate !== "home"){ ?>
		   
		    <div class="form-group">
		      	<strong>Content</strong>
		      	<textarea name="desc" id="" cols="30" rows="10" class="tinymc"><?php echo $page_detail['desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <?php
			}
			else{}
			?>
		   
		    <br>
		    <div>
		    	<!-- <a href="<?php echo base_url('about');?>" class="btn_cancel close_box">CANCEL</a> -->
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<script>
	$(function() {
	    $("#uploadFile").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });

	});
</script>