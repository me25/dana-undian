<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<?php $this->load->view('includes/head')?>
<body class="cover_login">
    <br><br><br><br><br><br>

	<div class="wp_login f15">
         
        <img src="<?php echo assets_url('images');?>/logo_dana.png" class="logo_login">
    	<form action="<?php echo base_url('auth');?>/process" method="post" class="login_box" autocomplete="off">
            <div class="text_input">
                <?php if ($this->session->flashdata('error')) { ?>
					<div class="notif">
					  <?php echo $this->session->flashdata('error')?>
					</div>
				<?php } ?>
                <strong>Email</strong>
            	<input type="text" class="input" placeholder="Alamat email" name="email" required="required" />
                <strong>Password</strong>
                <input type="password" class="input" placeholder="Password" name="password"  required="required"/>
                <input type="hidden" class="input" value="<?php echo $_GET['continue'];?>" name="continue"/>
                <strong></strong>
            </div>
            
            <input type="hidden" class="input95" name="url" value="<?php echo $_GET['continue']?>"/>
            <input type="submit" class="mt10 btn_login" value="LOGIN" />
        </form>
    </div>
<?php $this->load->view('includes/js')?>
</body>
</html>